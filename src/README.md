# <!=package.json name> <!=package.json version>

<!=package.json description>

[![pipeline status](https://gitlab.com/bytesnz/<!=package.json name>/badges/main/pipeline.svg)](<!=package.json repository.url>/commits/main)
[![<!=package.json name> on NPM](https://bytes.nz/b/<!=package.json name>/npm)](https://npmjs.com/package/<!=package.json name>)
[![license](https://bytes.nz/b/<!=package.json name>/custom?color=yellow&name=license&value=AGPL-3.0)](<!=package.json repository.url>/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/<!=package.json name>/custom?color=yellowgreen&name=development+time&value=~3+hours)](<!=package.json repository.url>/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/<!=package.json name>/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](<!=package.json repository.url>/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/<!=package.json name>/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Person-in-the-middle serial communications. Requires virtual com ports, like
those created by [com0com](http://com0com.sourceforge.net/) Windows or
socat on Linux.

# Usage

# Node Module

```js
import { createSpy } from 'serial-spy';

const spy = createSpy({
	port1: '/dev/tty0',
	baudrate1: 19200,
	port2: '/dev/tty1',
	baudrate2: 19200
});

spy.on('error', (error, port) => console.error('Error on port', port, error));

spy.on('data', (data, port) => console.log('Data on port', port, data));
```

# Command Line Interface

```
Usage:
serial-spy [-h] [-1 <port1>[,<baudrate1>] [-2 <port1>[,<baudrate2>]]

  -1 <port1>[,<baudrate1>]   The first port (and optionally baudrate) to
      person-in-the-middle
  -2 <port2>[,<baudrate2>]   The second port (and optionally baudrate) to
      person-in-the-middle
  -H,--hex                   Display all data as hex
  -h,--help                  Show this help
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

<!=CHANGELOG.md>

[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
