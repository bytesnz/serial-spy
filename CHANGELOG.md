# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2022-06-30

### Fixed

- Fixed errors due to undefined variables in `serial-mitm` command

### Added

- Port labels to `serial-mitm` command by adding third optional port argument
  e.g. `-1 /dev/tty0,9600,Device1`. The labels will be used in the logs output
  by `serial-mitm`

## [0.1.0] - 2022-06-15

Initial version!

[0.2.0]: https://gitlab.com/bytesnz/serial-mitm/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/bytesnz/serial-mitm/-/tree/v0.1.0
