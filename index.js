import { SerialPort } from 'serialport';
import chalk from 'chalk';

const ports = await SerialPort.list();

/** @typedef {Object} Options
 *
 * @property {string} port1 Port 1
 * @property {string} port2 Port 2
 * @property {number} baud1 Port 1 baudrate
 * @property {number} baud2 Port 2 baudrate
 * @property {boolean} forwardPort1Data If false, won't forward port1 data
 *   to port2 (default: true)
 * @property {boolean} forwardPort2Data If false, won't forward port2 data
 *   to port1 (default: true)
 */

/** @callback PortDataEventHandlerChange
 *
 * @param {'port1Data'|'port2Data'} event
 * @param {(data: Buffer) => void} handler
 */

/** @callback DataEventHandlerChange
 *
 * @param {'data'} event
 * @param {(data: Buffer, port: 1|2) => void} handler
 */

/** @callback PortErrorEventHandlerChange
 *
 * @param {'port1Error'|'port2Error'} event
 * @param {(error: Buffer) => void} handler
 */

/** @callback ErrorEventHandlerChange
 *
 * @param {'error'} event
 * @param {(error: Buffer, port: 1|2) => void} handler
 */

/** @typedef {PortDataEventHandlerChange|DataEventHandlerChange|PortErrorEventHandlerChange|ErrorEventHandlerChange} HandlerChange */

/**
 * Create a serialSpy
 *
 * @param {Options} options Serial spy options
 */
export const createSpy = (options) => {
	const eventHandlers = {
		data: [],
		port1Data: [],
		port2Data: [],
		error: [],
		port1Error: [],
		port2Error: []
	};

	const port1 = new SerialPort({ path: options.port1, baudRate: options.baud1 });
	const port2 = new SerialPort({ path: options.port2, baudRate: options.baud2 });

	port1.on('error', (error) => {
		if (!eventHandlers.port1Error.length && !eventHandlers.error.length) {
			throw error;
		}

		if (eventHandlers.port1Error.length) {
			for (let handler of eventHandlers.port1Error) {
				handler(error);
			}
		}

		if (eventHandlers.error.length) {
			for (let handler of eventHandlers.error) {
				handler(error, 1);
			}
		}
	});

	port2.on('error', (error) => {
		if (!eventHandlers.port2Error.length && !eventHandlers.error.length) {
			throw error;
		}

		if (eventHandlers.port2Error.length) {
			for (let handler of eventHandlers.port2Error) {
				handler(error);
			}
		}

		if (eventHandlers.error.length) {
			for (let handler of eventHandlers.error) {
				handler(error, 2);
			}
		}
	});

	port1.on('data', (data) => {
		if (options.forwardPort1Data !== false) {
			port2.write(data);
		}

		if (eventHandlers.port1Data.length) {
			for (let handler of eventHandlers.port1Data) {
				handler(data);
			}
		}

		if (eventHandlers.data.length) {
			for (let handler of eventHandlers.data) {
				handler(data, 1);
			}
		}
	});

	port2.on('data', (data) => {
		if (options.forwardPort2Data !== false) {
			port1.write(data);
		}

		if (eventHandlers.port2Data.length) {
			for (let handler of eventHandlers.port2Data) {
				handler(data);
			}
		}

		if (eventHandlers.data.length) {
			for (let handler of eventHandlers.data) {
				handler(data, 2);
			}
		}
	});

	return {
		/// Port1 SerialPort instance
		port1,
		/// Port2 SerialPort instance
		port2,
		/** @type {HandlerChange}
		 * Add an event handler
		 */
		on: (event, handler) => {
			if (!eventHandlers[event]) {
				throw new Error(`Unknown event ${event}`);
			}

			if (eventHandlers[event].indexOf(handler) === -1) {
				eventHandlers[event].push(handler);
			}
		},
		/** @type {HandlerChange}
		 * Remove an event handler
		 */
		off: (event, handler) => {
			if (!eventHandlers[event]) {
				throw new Error(`Unknown event ${event}`);
			}

			const index = eventHandlers[event].indexOf(handler);

			if (index !== -1) {
				eventHandlers[event].splice(index, 1);
			}
		},
		/**
		 * Close the serial ports
		 */
		close: () => {
			port1.close();
			port2.close();
		}
	};
};

/**
 * Format the given data to output to the console using formatChar
 *
 * @param {Iterable<number>} data Data to format
 * @param {boolean} hex If true, all data will be output as hex characters
 *
 * @see formatChar
 */
export const formatData = (data, hex) => {
	let string = '';

	for (let c of data) {
		string += formatChar(c);
	}

	return string;
};

/**
 * Format the given character to output to the console. If the character
 * is a character, print it, if it is a newline or a carriage return, print
 * it as its greyed escape character, otherwise print it as red hex
 *
 * @param {number} c Character to print
 * @param {boolean} hex If true, all data will be output as hex characters
 */
export const formatChar = (c, hex) => {
	if (!hex && c >= 32 && c <= 126) {
		return String.fromCharCode(c);
	} else if (!hex && c == 0x0d) {
		return chalk.grey('\\r');
	} else if (!hex && c == 0x0a) {
		return chalk.grey('\\n');
	} else {
		const hex = c.toString(16);
		return chalk.red((hex.length === 1 ? '0' : '') + hex);
	}
};
